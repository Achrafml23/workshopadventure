# Gitlab

## .gitlab-ci.yml cheat sheet

https://medium.com/open-devops-academy/cheat-sheet-for-devops-gitlab-ci-yml-gitlab-2ffbf0c4f7ac

## Comment générer ma clef SSH

```sh
# Pour une clef ed25519 (pour une clef rsa, juste remplacer par rsa, les valeurs possibles sont dsa | ecdsa | ecdsa-sk | ed25519 | ed25519-sk | rsa)
$ ssh-keygen -t ed25519
Generating public/private ed25519 key pair.
Enter file in which to save the key (/home/gitpod/.ssh/id_ed25519):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/gitpod/.ssh/id_ed25519
Your public key has been saved in /home/gitpod/.ssh/id_ed25519.pub
```

## Où ajouter ma clef SSH

- Aller dans **Préférence** (en haut à droite sur votre avatar)
- Aller dans **SSH Keys**
- Copier-coller le contenu de votre clef **publique** `$HOME/id_*.pub` dans le bloc **Key**
- Cliquer sur **Add key**

## Comment forker un repo

- Aller sur la page du repo à forker, ex `https://gitlab.com/gitops-heros/workshopadventure`
- Cliquer sur le bouton **Fork** en haut à droite
- Selectionner dans le **namespace** votre utilisateur
- Selectionner **Public** si votre repo doit l'être
- Cliquer sur le bouton bleu **Fork Project**

## Comment cloner un repo

- Aller sur la page de votre repo, ex `https://gitlab.com/<mon username>/workshopadventure`
- Cliquer sur le bouton bleu **Clone**
- Selectionner l'url marquée **Clone with SSH**
- Dans une console lancer la commande suivante `git clone git@gitlab.com:<mon username>/workshopadventure.git`

## Comment ouvrir mon repo sur GitPod

- Aller sur la page de votre repo, ex `https://gitlab.com/<mon username>/workshopadventure`
- Juste à côté du bouton bleu **Clone** il y a une liste déroulante avec soit **Web IDE** soit **Gitpod**
- Positionner cette liste déroulante sur **Gitpod**
- Cliquer dessus et un onglet devrait s'ouvrir avec votre repo sur Gitpod

## Comment voir mes Runners

- Aller sur gitlab sur votre repo, ex `https://gitlab.com/<mon username>/workshopadventure`
- Aller dans **Settings**->**CI/CD** (barre de gauche)
- Ouvrir le volet **Runners** en cliquant sur **Expand**

## Comment voir mes variables

- Aller sur gitlab sur votre repo, ex `https://gitlab.com/<mon username>/workshopadventure`
- Aller dans **Settings**->**CI/CD** (barre de gauche)
- Ouvrir le volet **Variables** en cliquant sur **Expand**
