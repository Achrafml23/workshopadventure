#!/bin/bash

# echo "****************************************************************************************************"
# echo "Before you need to manually install vault"
# echo "First install vault"
# echo "----------------------------------------------------------------------------------------------------"

## 
set -e
DIR=$(dirname "$0")

echo "🏗️ Installing BanzaiCloud Vault-Operator"
kubectl create ns vault-operator
kubectl label namespace vault-operator name=vault-operator
kubectl apply -n vault-operator -f vault-operator
kubectl apply -n vault-operator -f rbac.yaml
kubectl apply -n vault-operator -f cr.yaml
kubectl apply -n vault-operator -f vault-secrets-webhook

echo "🔗 Waiting until available"
until kubectl -n vault-operator get pod -l app.kubernetes.io/name="vault" -o go-template='{{.items | len}}' | grep -qxF 1; do
    echo -n "."
    sleep 2
done
echo
kubectl wait pod vault-0 -n vault-operator --timeout=-1s --for=jsonpath='{.status.phase}'=Running

sleep 10

kubectl -n vault-operator port-forward vault-0 8200 &
PID=$!

export VAULT_ADDR=https://127.0.0.1:8200
kubectl -n vault-operator get secret vault-tls -o jsonpath="{.data.ca\.crt}" | base64 --decode > $PWD/vault-ca.crt
export VAULT_CACERT=$PWD/vault-ca.crt

vault status

export VAULT_TOKEN=$(kubectl -n vault-operator get secrets vault-unseal-keys -o jsonpath={.data.vault-root} | base64 --decode)

echo "🔗 Creating james-secrets"
vault kv put secret/james-secrets content='Salam Agadir 2022 :)'

kill ${PID}
